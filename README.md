
Frontend Developer - Coding Challenge The Task
===================

Frontend Developer - Coding Challenge The Task 0.1.0 - React, bootstrap and many other latest technologies.

## Quick start

1.  Make sure that you have Node v8 or above installed.
2.  Clone this repo using `git clone git@gitlab.com:syednomansd/chemondis-frontend-test.git`
3.  Move to the appropriate directory: `cd chemondis-frontend-test`
4.  Run `npm install` in order to install dependencies.

### Local Environment with Live Reload

```
npm start
```

_At this point you can see the example app at `http://localhost:3000`._

### Build for Production

```
npm run build
```

## Technology stack

| Name | Version |
| ------ | ------ |
| React | `16` |
| Babel | `6` |
| Bootstrap | `4` |

## Task done

| Task | Action |
| ------ | ------ |
| Browser back button functionality | `Done` |
| Readable and tested code | `Done` |
| Coding not just the "happy path" but also handling missing data, network issues, ... | `Done` |
| A performant solution (amount of requests & bytes, compression, render-blocking Js, lazy load...) | `Done` |
| Mobile first approach | `Done` |
| Clean and responsive layout | `Done` |
| An informative commit history | `I made this application in one shot so there is no commit history` |
| To run the project via docker | `I will do this later` |
| Documentation | `Done` |


**Note:** This Project is modified version of awesome [Create React App](https://github.com/facebook/create-react-app). I had modified it according to mine Frontend Developer - Coding Challenge The Task 0.1.0 project needs.
