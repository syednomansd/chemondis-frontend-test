import React, {PropTypes} from 'react';

const User = (props) => {
	const user = props.user;

	return (
		<div className="card" >
			<button onClick={props.handleClick} className="text-left">
				<span className="small">#{user.id}</span> {user.name}
			</button>
		</div>
	);
};

User.propTypes = {
	user: PropTypes.object,
	handleClick: PropTypes.func
};

export default User;
